package com.sp.openhis.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sp.openhis.domain.VerificationCode;

/**
* @author Administrator
* @description 针对表【his_verification_code】的数据库操作Service
* @createDate 2023-03-23 09:51:39
*/
public interface VerificationCodeService extends IService<VerificationCode> {
    int sendSms( String phoneNumber);

    VerificationCode checkcode(String phoneNumber,Integer code);
}
