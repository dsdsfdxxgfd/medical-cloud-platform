package com.sp.openhis.impl;


import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sp.openhis.config.SmsConfig;
import com.sp.openhis.domain.VerificationCode;
import com.sp.openhis.mapper.VerificationCodeMapper;
import com.sp.openhis.service.VerificationCodeService;

import com.sp.openhis.utils.HttpUtils;
import com.sp.openhis.utils.IdGeneratorSnowFlake;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Method;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
* @author Administrator
* @description 针对表【his_verification_code】的数据库操作Service实现
* @createDate 2023-03-23 09:51:39
*/
@Service(timeout = 5000,methods = {@Method(name="sendSms",retries = 0)})
public class VerficationCodeServiceImpl extends ServiceImpl<VerificationCodeMapper, VerificationCode> implements VerificationCodeService {
     @Autowired
     private VerificationCodeMapper verificationCodeMapper;
     @Autowired
     private SmsConfig smsConfig;

    /**
     * 发送短信
     * @param phoneNumber
     * @return
     */
    @Override
    public int sendSms(String phoneNumber){
        Integer code = IdGeneratorSnowFlake.generateVerificationCode();
        try {
            String result = this.execute(phoneNumber,code);
            JSONObject jsonObject = (JSONObject)JSON.parse(result);
            if(jsonObject != null && StringUtils.isNotEmpty(jsonObject.getString("respCode"))){
                if(jsonObject.getString("respCode").equals("0000")){
                    return saveVerification(phoneNumber,code);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public VerificationCode checkcode(String phoneNumber, Integer code) {
        QueryWrapper<VerificationCode> wrapper = new QueryWrapper<>();
        wrapper.eq(VerificationCode.PHONE_NUMBER,phoneNumber);
        wrapper.eq(VerificationCode.VERIFICATION_CODE,code);
        wrapper.eq(VerificationCode.IS_CHECK,0);
        return verificationCodeMapper.selectOne(wrapper);
    }

    /**
     * 发送成功后把验证码保存到his_Verification_code表
     * @param phoneNumber
     * @param code
     * @return
     */
    private int saveVerification(String phoneNumber, Integer code) {
        VerificationCode verificationCode = new VerificationCode();
        verificationCode.setPhoneNumber(phoneNumber);
        verificationCode.setVerificationCode(code);
        verificationCode.setCreateTime(DateUtil.date());
        verificationCode.setIsCheck(0);
        return verificationCodeMapper.insert(verificationCode);
    }

    /**
     * 短信发送
     * https://openapi.danmi.com/distributor/sendSMS
     * @return
     */
    public String execute(String phoneNumber ,Integer code){
        StringBuffer buffer = new StringBuffer();
        buffer.append("accountSid").append("=").append(smsConfig.getACCOUNT_SID());
        buffer.append("&to").append("=").append(phoneNumber);
        buffer.append("templateid").append("=").append(smsConfig.getTEMPLATE_ID());
        try {
            buffer.append("&param").append("=").append(URLEncoder.encode(code.toString(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        String body=buffer.toString()+smsConfig.createCommonParam(smsConfig.getACCOUNT_SID(),smsConfig.getAUTH_TOKEN());
        return HttpUtils.post(smsConfig.getBASE_URL(),body);
    }
}




