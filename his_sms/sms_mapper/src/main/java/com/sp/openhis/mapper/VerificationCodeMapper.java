package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.VerificationCode;


/**
* @author Administrator
* @description 针对表【his_verification_code】的数据库操作Mapper
* @createDate 2023-03-23 09:51:39
* @Entity com.sp.poenhis.domain.VerificationCode
*/
public interface VerificationCodeMapper extends BaseMapper<VerificationCode> {

}




