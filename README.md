# 医疗云平台

#### 介绍
分布式医疗云平台系统是以完整的基层医疗机构信息化解决方案为出发点，打造链接诊所、医生、患者、一站式互联网医疗服务系统，深度挖掘了基层医疗机构需求，解决其真正痛点，提供医疗前沿资源及信息共享等、全面提升医疗管理质量，可执行落地的综合性解决方案
核心模块功能介绍


#### 技术选型
Spring Boot + Mybatis Plus +Shiro+ Nacos + Sentinel + Dubbo + RocketMQ + Redis + Hutool + Maven + Fastdfs + Swagger-ui + Mycat + Docker

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
