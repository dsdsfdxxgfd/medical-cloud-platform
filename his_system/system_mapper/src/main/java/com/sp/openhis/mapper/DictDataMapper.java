package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sp.openhis.domain.DictData;
import com.sp.openhis.vo.DataGridView;

public interface DictDataMapper extends BaseMapper<DictData> {


}
