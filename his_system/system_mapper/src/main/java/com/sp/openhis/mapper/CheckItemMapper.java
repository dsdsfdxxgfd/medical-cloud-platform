package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.sp.openhis.domain.CheckItem;

/**
* @author a
* @description 针对表【sys_check_item(检查费用表)】的数据库操作Mapper
* @Entity com.itbaizhan.openhis.domain.CheckItem
*/
public interface CheckItemMapper extends BaseMapper<CheckItem> {

}




