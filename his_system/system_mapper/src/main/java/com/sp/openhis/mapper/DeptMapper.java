package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.Dept;

/**
* @author a
* @description 针对表【sys_dept(部门/科室表)】的数据库操作Mapper
* @Entity com.itbaizhan.openhis.domain.Dept
*/
public interface DeptMapper extends BaseMapper<Dept> {

}




