package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.OperLog;


/**
* @author a
* @description 针对表【sys_oper_log(操作日志记录)】的数据库操作Mapper
* @Entity com.itbaizhan.openhis.domain.OperLog
*/
public interface OperLogMapper extends BaseMapper<OperLog> {

}




