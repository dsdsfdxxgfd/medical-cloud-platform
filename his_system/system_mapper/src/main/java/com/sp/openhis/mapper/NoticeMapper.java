package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.sp.openhis.domain.Notice;

/**
* @author a
* @description 针对表【sys_notice(通知公告表)】的数据库操作Mapper
* @Entity com.itbaizhan.openhis.domain.Notice
*/
public interface NoticeMapper extends BaseMapper<Notice> {

}




