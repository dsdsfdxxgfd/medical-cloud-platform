package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.User;


/**
* @author Administrator
* @description 针对表【sys_user(用户信息表)】的数据库操作Mapper
* @createDate 2023-03-20 15:27:26
* @Entity com.sp.poenhis.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




