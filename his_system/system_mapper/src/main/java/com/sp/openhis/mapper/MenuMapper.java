package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.Menu;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
* @author Administrator
* @description 针对表【sys_menu(菜单权限表)】的数据库操作Mapper
* @createDate 2023-03-22 10:38:27
* @Entity com.sp.poenhis.domain.Menu
*/
public interface MenuMapper extends BaseMapper<Menu> {
    List<Long> queryMenuIdsByRoleId(@Param("roleId") Long roleId);

    Long queryChildCountByMenuId(@Param("menuId") Long menuId);

}




