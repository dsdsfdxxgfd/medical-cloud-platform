package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.LoginInfo;


/**
* @author Administrator
* @description 针对表【sys_login_info(系统访问记录)】的数据库操作Mapper
* @createDate 2023-03-22 10:38:27
* @Entity com.sp.poenhis.domain.LoginInfo
*/
public interface LoginInfoMapper extends BaseMapper<LoginInfo> {

}




