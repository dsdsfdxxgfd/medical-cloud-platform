package com.sp.openhis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sp.openhis.domain.DictType;


/**
* @author Administrator
* @description 针对表【sys_dict_type(字典类型表)】的数据库操作Mapper
* @createDate 2023-04-03 11:13:10
* @Entity com.sp.openhis.domain.SysDictType
*/
public interface DictTypeMapper extends BaseMapper<DictType> {

}




