package com.sp.openhis.controller;

import com.sp.openhis.dto.DictDataDto;
import com.sp.openhis.dto.DictTypeDto;
import com.sp.openhis.service.DictDataService;
import com.sp.openhis.service.DictTypeService;
import com.sp.openhis.vo.AjaxResult;
import com.sp.openhis.vo.DataGridView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("system/dict/type")
public class DictTypeController {
    @Autowired
    private DictTypeService dictTypeService;

    /**
     * 分页查询字典类型
     * @param dictTypeDto
     * system/dict/type/listForPage?pageNum=1&pageSize=10
     * @return
     */
    @GetMapping("/listForPage")
    private AjaxResult listForpage(DictTypeDto dictTypeDto){
        DataGridView dataGridView=dictTypeService.listForPage(dictTypeDto);
        return AjaxResult.success("查询成功",dataGridView.getData(),dataGridView.getTotal());
    }

    /**
     *同步字典数据
     * @return
     */
    @GetMapping("dictCacheAsync")
    public AjaxResult dictCacheAsync(){
        dictTypeService.dictCacheAsync();
        return AjaxResult.success();
    }
}
