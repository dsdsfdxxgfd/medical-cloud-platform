package com.sp.openhis.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.sp.openhis.domain.LoginInfo;
import com.sp.openhis.dto.LoginInfoDto;
import com.sp.openhis.vo.DataGridView;

/**
* @author Administrator
* @description 针对表【sys_login_info(系统访问记录)】的数据库操作Service
* @createDate 2023-03-22 10:38:27
*/
public interface LoginInfoService extends IService<LoginInfo> {
    int insterLonginInfo(LoginInfo loginInfo);
    DataGridView listForPage(LoginInfoDto loginInfoDto);

    int deleteLoginInfoByIds(Long[] infoIds);

    int clearLoginInfo();
}
