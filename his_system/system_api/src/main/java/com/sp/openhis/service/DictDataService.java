package com.sp.openhis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sp.openhis.domain.DictData;
import com.sp.openhis.dto.DictDataDto;
import com.sp.openhis.vo.DataGridView;

import java.util.List;

/**
 * @author a
 * @description 针对表【sys_dict_data(字典数据表)】的数据库操作Service
 */
public interface DictDataService extends IService<DictData> {
    DataGridView listForPage(DictDataDto dto);

    DictData selectDictDataById(Long dictid);

    int inster(DictDataDto dto);

    int update(DictDataDto dictDataDto);
    int deleteDictData(Long[] dictIds);
    List<DictData> selectDictDataByDictType(String dictType);
}
