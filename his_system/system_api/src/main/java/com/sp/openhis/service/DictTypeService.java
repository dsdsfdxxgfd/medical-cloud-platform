package com.sp.openhis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sp.openhis.domain.DictType;
import com.sp.openhis.dto.DictDataDto;
import com.sp.openhis.dto.DictTypeDto;
import com.sp.openhis.vo.DataGridView;

/**
* @author Administrator
* @description 针对表【sys_dict_type(字典类型表)】的数据库操作Service
* @createDate 2023-04-03 11:13:10
*/
public interface DictTypeService extends IService<DictType> {
    DataGridView listForPage(DictTypeDto dictTypeDto);

    void dictCacheAsync();

}
