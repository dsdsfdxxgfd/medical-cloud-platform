package com.sp.openhis.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sp.openhis.domain.OperLog;
import com.sp.openhis.dto.OperLogDto;
import com.sp.openhis.vo.DataGridView;

/**
* @author a
* @description 针对表【sys_oper_log(操作日志记录)】的数据库操作Service
*/
public interface OperLogService extends IService<OperLog> {

    DataGridView listForPage(OperLogDto operLogDto);

    int deleteOperLogByIds(Long[] infoIds);

    int clearAllOperLog();

    int insertOperLog(OperLog operLog);

}
