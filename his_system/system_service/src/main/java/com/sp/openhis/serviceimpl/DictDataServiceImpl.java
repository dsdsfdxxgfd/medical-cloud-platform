package com.sp.openhis.serviceimpl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sp.openhis.constants.Constants;
import com.sp.openhis.domain.DictData;
import com.sp.openhis.dto.DictDataDto;
import com.sp.openhis.mapper.DictDataMapper;
import com.sp.openhis.service.DictDataService;
import com.sp.openhis.vo.DataGridView;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Arrays;
import java.util.List;

@Service
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictData> implements DictDataService{

     @Autowired
     private DictDataMapper dictDataMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Override
    public DataGridView listForPage(DictDataDto dto) {
        Page<DictData> page = new Page<>(dto.getPageNum(), dto.getPageSize());
        QueryWrapper<DictData> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(dto.getDictType()),
                DictData.COL_DICT_TYPE,dto.getDictType());
        wrapper.eq(StringUtils.isNotBlank(dto.getDictLabel()),
                DictData.COL_DICT_LABEL,dto.getDictLabel());
        wrapper.eq(StringUtils.isNotBlank(dto.getStatus()),
                DictData.COL_STATUS,dto.getStatus());
        dictDataMapper.selectPage(page,wrapper);
        return new DataGridView(page.getTotal(),page.getRecords());
    }

    @Override
    public DictData selectDictDataById(Long dictid) {
        return dictDataMapper.selectById(dictid);
    }

    @Override
    public int inster(DictDataDto dto) {
        DictData dictData = new DictData();
        BeanUtil.copyProperties(dto,dictData);
        dictData.setCreateBy(dto.getSimpleUser().getUserName());
        dictData.setCreateTime(DateUtil.date());
        return dictDataMapper.insert(dictData);
    }

    @Override
    public int update(DictDataDto dictDataDto) {
        DictData dictData = new DictData();
        BeanUtil.copyProperties(dictDataDto,dictData);
        dictData.setUpdateBy(dictDataDto.getSimpleUser().getUserName());
        return dictDataMapper.updateById(dictData);
    }

    @Override
    public int deleteDictData(Long[] dictIds) {
        List<Long> ids = Arrays.asList(dictIds);
        if(ids != null && ids.size() > 0){
            return dictDataMapper.deleteBatchIds(ids);
        }else{
            return 0;
        }
    }
    /**
     * 之前是从数据库里面查询
     * 因为我们做到redis的缓存，所以 现在要去redis里面去查询
     * @param dictType
     * @return
     */
    @Override
    public List<DictData> selectDictDataByDictType(String dictType) {
        String key= Constants.DICT_REDIS_PREFIX + dictType;
        //dict:sys_normal_disable
        ValueOperations<String, String> opsForValue = redisTemplate.opsForValue();
        String json = opsForValue.get(key);
        List<DictData> dictDatas= JSON.parseArray(json,DictData.class);
        return dictDatas;
    }


}
