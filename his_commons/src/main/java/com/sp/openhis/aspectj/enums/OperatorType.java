package com.sp.openhis.aspectj.enums;

public enum OperatorType {

    OTHER,

    MANAGE,

    MOBILE
}
