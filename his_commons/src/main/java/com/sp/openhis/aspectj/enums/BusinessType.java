package com.sp.openhis.aspectj.enums;

public enum BusinessType {

    OTHER,

    INSERT,

    UPADTE,

    DELETE,

    GRANT,

    EXPORT,

    IMPORT,

    CLEAN
}
