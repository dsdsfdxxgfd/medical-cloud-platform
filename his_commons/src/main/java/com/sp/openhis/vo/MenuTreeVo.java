package com.sp.openhis.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @Author:
 * 构造菜单返回给前台的vo
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuTreeVo {
  private String id;
  private String serPath;//菜单表里面的url
  //此处添加一个属性决定菜单栏是否显示
  private boolean show=true;
}


